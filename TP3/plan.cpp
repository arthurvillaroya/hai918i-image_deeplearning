#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

void intToBinaryArray(int number, int binaryArray[8]) {
    for (int i = 7; i >= 0; i--) {
        binaryArray[i] = (number >> i) & 1;
    }
}

int binaryArrayToInt(const int binaryArray[8]) {
    int result = 0;
    
    for (int i = 0; i < 8; i++) {
        result = (result << 1) | binaryArray[i];
    }
    
    return result;
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    int indexPlan;
    
    if (argc != 4){
        printf("Usage: ImageIn.pgm ImageOut.pgm K\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s", cNomImgLue) ;
    sscanf (argv[2],"%s", cNomImgEcrite);
    sscanf (argv[3],"%d", &indexPlan);
    

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    int val;
    int binaryVal[8];
    for(int i = 0; i <  nTaille; i++){
        intToBinaryArray(ImgIn[i], binaryVal);
        val = binaryVal[7 - indexPlan];
        if(val == 0){
            ImgOut[i] = 0;
        }else{
            ImgOut[i] = 255;
        }
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

   return 0;
}