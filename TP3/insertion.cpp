#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "image_ppm.h"

void intToBinaryArray(int number, int binaryArray[8]) {
    for (int i = 7; i >= 0; i--) {
        binaryArray[i] = (number >> i) & 1;
    }
}

int binaryArrayToInt(const int binaryArray[8]) {
    int result = 0;
    
    for (int i = 0; i < 8; i++) {
        result = (result << 1) | binaryArray[i];
    }
    
    return result;
}

char chiffrementDeCesar(char caractere, int decalage) {
    if (std::isalpha(caractere)) {
        char base = (std::isupper(caractere)) ? 'A' : 'a';
        return static_cast<char>((caractere - base + decalage) % 26 + base);
    }
    return caractere;
}

char binaryToChar(const int binaryArray[8]) {
    int buff = 0;
    for(int i = 0; i < 8; i++){
        buff += std::pow(2,i) * binaryArray[i];
    }

    return buff;
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250], nomFicher[250];
    int nH, nW, nTaille;
    
    if (argc != 4){
        printf("Usage: ImageIn.pgm ImageOut.pgm K\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s", cNomImgLue) ;
    sscanf (argv[2],"%s", cNomImgEcrite);
    sscanf (argv[3],"%s", nomFicher);
    
    //récup et chiffrement du fichier
    std::ifstream fichier(nomFicher);

    if (!fichier) {
        std::cerr << "Impossible d'ouvrir le fichier." << std::endl;
        return 1;
    }

    std::vector<char> text;
    std::string ligne;
    while (std::getline(fichier, ligne)) {
        for(int i = 0; i < ligne.length(); i++){
            text.push_back(chiffrementDeCesar(ligne[i], 0));
        }
    }

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    int val;
    int binaryValImg[8];
    int binaryValText[8];
    int cptChar = 0;
    std::string textTe;

    std::cout<<text.size()<<std::endl;
    int mask = (1<<7)^255;
    for(int i = 0; i < nTaille; i++){
        if(i/8 < text.size()){
            /*intToBinaryArray(ImgIn[i], binaryValImg);
            intToBinaryArray(lignesChiffrees[i/7], binaryValText);
            binaryValImg[0] = binaryValText[cptChar];
            int valSortie = binaryArrayToInt(binaryValImg);*/
            //std::cout<<valSortie<<std::endl;
            intToBinaryArray(text[i/8], binaryValText);
            ImgOut[i] = (ImgIn[i] & mask) | (binaryValText[cptChar]<<7);
            if(cptChar == 7){
                cptChar=-1;
                textTe.push_back(binaryToChar(binaryValText));
            }
            cptChar++;
        }
        else{
           int newVal = rand() % 2;
           /*intToBinaryArray(ImgIn[i], binaryValImg);
           binaryValImg[0] = newVal;
           int valSortie = binaryArrayToInt(binaryValImg);*/
           ImgOut[i] = (ImgIn[i] & mask) | (newVal<<7);//valSortie;  
        }
    }

    std::ofstream fichierSortie("text.txt");
    if (fichierSortie.is_open()) {
        fichierSortie << textTe << std::endl;
        fichierSortie.close();

        std::cout << "Le texte a été inséré dans le fichier avec succès." << std::endl;
    } else {
        // Erreur lors de l'ouverture du fichier
        std::cerr << "Impossible d'ouvrir le fichier en écriture." << std::endl;
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

   return 0;
}