#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <bitset>
#include "image_ppm.h"

void intToBinaryArray(int number, std::vector<int> binaryArray) {
    for (int i = 7; i >= 0; i--) {
        binaryArray[i] = number % 2;
        number /= 2;
    }
}

int binaryArrayToInt(std::vector<int> binaryArray) {
    int result = 0;
    
    for (int i = 0; i < 8; i++) {
        result = (result << 1) | binaryArray[i];
    }
    
    return result;
}

char binaryToChar(std::vector<int>  binaryArray) {
    int buff = 0;
    for(int i = 0; i < 8; i++){
        buff += std::pow(2,i) * binaryArray[i];
    }

    return buff;
}


char chiffrementDeCesar(char caractere, int decalage) {
    if (std::isalpha(caractere)) {
        char base = (std::isupper(caractere)) ? 'A' : 'a';
        return static_cast<char>((caractere - base + decalage) % 26 + base);
    }
    return caractere;
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250], nomFicher[250];
    int nH, nW, nTaille, bit, tailleText;
    
    if (argc != 5){
        printf("Usage: ImageIn.pgm bit tailleText\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s", cNomImgLue) ;
    sscanf (argv[2],"%s", nomFicher);
    sscanf (argv[3],"%d", &bit);
    sscanf (argv[4],"%d", &tailleText);

    std::cout<<tailleText<<std::endl;
    
    OCTET *ImgIn;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    int val;
    std::vector<int> binaryValImg; binaryValImg.resize(8);
    std::vector<int> binaryValText; binaryValText.resize(8);
    int cptChar = 0;
    std::string text;
    
    for(int i = 0; i < nTaille; i=i+8){
        if(i < tailleText*8){
            for(int j = 0; j < 8; j++){
                intToBinaryArray(ImgIn[i+j], binaryValImg);
               // std::cout<<"COUCOUY SALOPE"<<std::endl;
                binaryValText[j] = binaryValImg[10];
                //std::cout<<"COUCOUY SALOPE23"<<std::endl;
            }
            char lettre = binaryArrayToInt(binaryValText);
            text.push_back(lettre);
        }
    }

    std::ofstream fichierSortie(nomFicher);
    if (fichierSortie.is_open()) {
        fichierSortie << text << std::endl;
        fichierSortie.close();

        std::cout << "Le texte a été inséré dans le fichier avec succès." << std::endl;
    } else {
        // Erreur lors de l'ouverture du fichier
        std::cerr << "Impossible d'ouvrir le fichier en écriture." << std::endl;
    }

    free(ImgIn);

   return 0;
}