#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "image_ppm.h"

void intToBinaryArray(int number, int binaryArray[8]) {
    for (int i = 7; i >= 0; i--) {
        binaryArray[i] = (number >> i) & 1;
    }
}

int binaryArrayToInt(const int binaryArray[8]) {
    int result = 0;
    
    for (int i = 0; i < 8; i++) {
        result = (result << 1) | binaryArray[i];
    }
    
    return result;
}

char chiffrementDeCesar(char caractere, int decalage) {
    if (std::isalpha(caractere)) {
        char base = (std::isupper(caractere)) ? 'A' : 'a';
        return static_cast<char>((caractere - base + decalage) % 26 + base);
    }
    return caractere;
}

char binaryToChar(const int binaryArray[8]) {
    int buff = 0;
    for(int i = 0; i < 8; i++){
        buff += std::pow(2,i) * binaryArray[i];
    }

    return buff;
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    if (argc != 3){
        printf("Usage: ImageIn.pgm ImageOut.pgm K\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s", cNomImgLue) ;
    sscanf (argv[2],"%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    int val;
    int binaryValImg[8];
    int binaryValText[8];

    int mask = (1<<0)^255;
    for(int i = 0; i < nTaille; i++){
        int newVal = rand() % 2;
        ImgOut[i] = (ImgIn[i] & mask) | (newVal<<0);//valSortie;  
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

   return 0;
}