#include <igl/opengl/glfw/Viewer.h>
#include <igl/readOBJ.h>
#include <Eigen/Dense>
#include <iostream>

std::vector<Eigen::Vector3d> matrixToVectorOfVectors(const Eigen::MatrixXd& matrix) {
  std::vector<Eigen::Vector3d> vectorOfVectors;

  int numRows = matrix.rows();

  for (int row = 0; row < numRows; row++) {
    Eigen::Vector3d vector3d(matrix(row, 0), matrix(row, 1), matrix(row, 2));
    vectorOfVectors.push_back(vector3d);
  }

  return vectorOfVectors;
}

void vectortoMatrix(Eigen::MatrixXd& matrix, const std::vector<Eigen::Vector3d> &vectors) {
  int numRows = matrix.rows();

  for (int i = 0; i < numRows; i++) {
    for (int j = 0; j < 3; j++) {
      matrix(i, j) = vectors[i](j);
    }
  }
}

Eigen::Vector3d getCentroid(const std::vector<Eigen::Vector3d> &mesh){
  Eigen::Vector3d center = Eigen::Vector3d(0,0,0);
  
  size_t nbVertex = mesh.size();
  for(size_t i = 0; i < nbVertex; i++){
    center += mesh[i];
  }

  return center/(double)nbVertex;
}

Eigen::Vector3d cartesianToSpherical(const Eigen::Vector3d &vertex, const Eigen::Vector3d &center){
  double rho = sqrt(std::pow((vertex(0) - center(0)),2) + std::pow((vertex(1) - center(1)),2) + std::pow(vertex(2) - center(2),2));
  double theta = atan2((vertex(1) - center(1)), (vertex(0) - center(0)));
  double phi = acos((vertex(2) - center(2))/rho);

  return Eigen::Vector3d(rho,theta,phi);
}

Eigen::Vector3d sphericalToCartesian(const Eigen::Vector3d &spherical, const Eigen::Vector3d &center) {
  double rho = spherical(0);
  double theta = spherical(1);
  double phi = spherical(2);

  double x = rho * sin(phi) * cos(theta) + center(0);
  double y = rho * sin(phi) * sin(theta) + center(1);
  double z = rho * cos(phi) + center(2);

  return Eigen::Vector3d(x, y, z);
}

std::vector<double> rhos(const std::vector<Eigen::Vector3d> mesh){
  std::vector<double> rhos(mesh.size());
  Eigen::Vector3d center = getCentroid(mesh);

  for(size_t i = 0; i < mesh.size(); i++){
    rhos[i] = mesh[i](0);
  }

  return rhos;
}

double minRad(const std::vector<double> &bin){
  size_t nbVertex = bin.size();
  double min = 10000000;

  for(size_t i = 0; i < nbVertex; i++){
    if(min > bin[i]){min = bin[i];}
  }

  return min;
}

double maxRad(const std::vector<double> &bin){
  size_t nbVertex = bin.size();
  double max = -1;

  for(size_t i = 0; i < nbVertex; i++){
    if(max < bin[i]){max = bin[i];}
  }

  return max;
}

void normalizeRhosBin(const std::vector<double> &bin, std::vector<double> &normalizedRhos){
  double min = minRad(bin);
  double max = maxRad(bin);
  normalizedRhos.resize(bin.size());

  double normalizedValue; 

  for (size_t i = 0; i < bin.size(); i++) {
    normalizedValue = (bin[i] - min) / (max - min);
    normalizedRhos[i] = normalizedValue;
  }
}


void deNormalizeRhosBin(std::vector<double> &bin, const std::vector<double> normalizedRhos, int i){
  double min = minRad(bin);
  double max = maxRad(bin);

  for (size_t i = 0; i < bin.size(); i++) {
    bin[i] = normalizedRhos[i] * (max - min) + min;
  }
}

std::vector<std::vector<double>> constructBins(const std::vector<double> &rhos, int nbBits, std::vector<std::vector<int>> &indexBins){
  std::vector<double> normalizedRhos;
  normalizeRhosBin(rhos, normalizedRhos);
  std::vector<std::vector<double>> bins; bins.resize(nbBits);
  indexBins.resize(nbBits);

  double quelBin;
 
  for(size_t i = 0; i < rhos.size(); i++){
    quelBin = normalizedRhos[i];

    for(int j = 1; j <= nbBits; j++){
      if(quelBin < ((double)j)/nbBits){
        bins[j-1].push_back(rhos[i]);
        indexBins[j-1].push_back(i);
        break;
      }
    }
  }

  return bins;
}

/*std::vector<std::vector<double>> constructBins(const std::vector<double> &rhos, int nbBits, std::vector<std::vector<int>> &indexBins){
  std::vector<std::vector<double>> histogram(nbBits);
  indexBins.resize(nbBits);

    double minRadial = minRad(rhos);
    double maxRadial = maxRad(rhos);

    for (int i = 0; i < nbBits; ++i) {
        double Bmin = minRadial + ((maxRadial - minRadial) / nbBits) * i;
        double Bmax = minRadial + ((maxRadial - minRadial) / nbBits) * (i + 1);

        for (unsigned int j = 0; j < rhos.size(); ++j) {
            if (rhos[j] > Bmin && rhos[j] <= Bmax) {
                histogram[i].push_back(rhos[j]);
                indexBins[i].push_back(j);
            }
        }
    }

  return histogram;
}*/

double moyBin(const std::vector<double> &bin){
  double moy = 0.0;

  for(size_t i = 0; i < bin.size(); i++){
    moy += bin[i];
  }

  return moy/bin.size();
}

std::vector<double> applyK(const std::vector<double> &bin, int k){
  std::vector<double> la(bin.size());
  
  for(size_t i = 0; i < bin.size(); i++){
    la[i] = std::pow(bin[i], k);
  }

  return la;
}

void insertMessage(Eigen::MatrixXd &V, const std::vector<int> &message, double alpha, double force){
  //MANIPULATION DU MESH ET RECUPERATION DES DISTANCES AU CENTRE
  std::vector<Eigen::Vector3d> meshVector = matrixToVectorOfVectors(V);
  Eigen::Vector3d centroid = getCentroid(meshVector);

  for(size_t i = 0; i < meshVector.size(); i++){
    meshVector[i] = cartesianToSpherical(meshVector[i], centroid);
  }
  
  std::vector<double> rhosA = rhos(meshVector);
  std::vector<std::vector<int>> indexBins;

  std::vector<std::vector<double>> bins = constructBins(rhosA, message.size(), indexBins);
  std::vector<std::vector<double>> normalizedBins(bins.size());
  
  for(size_t i = 0; i < bins.size(); i++){
    normalizeRhosBin(bins[i], normalizedBins[i]);
  }

  //INSERTION DU MESSAGE
  double k;

  for(int t = 0; t < message.size(); t++){
    k = 1;

    if(bins[t].size() > 0){
      if(message[t] == 0){
        while(moyBin(applyK(normalizedBins[t], k)) > (0.5 - alpha)){
          k += force;
        }
      }else{
        while(moyBin(applyK(normalizedBins[t], k)) < (0.5 + alpha)){
          k -= force;
        }
      }

      normalizedBins[t] = applyK(normalizedBins[t], k);
    }
  }

  for(size_t i = 0; i < bins.size(); i++){
    deNormalizeRhosBin(bins[i], normalizedBins[i], i);
  }


  for(size_t i = 0; i < bins.size(); i++){
    for(size_t j = 0; j < bins[i].size(); j++){
      int index = indexBins[i][j];
      meshVector[index](0) = bins[i][j];
    }
  }


  for(size_t i = 0; i < meshVector.size(); i++){
    meshVector[i] = sphericalToCartesian(meshVector[i], centroid);
  }


  vectortoMatrix(V, meshVector);
}

void extractMessage(Eigen::MatrixXd &V, std::vector<int> &message, double alpha){
  //MANIPULATION DU MESH ET RECUPERATION DES DISTANCES AU CENTRE
  std::vector<Eigen::Vector3d> meshVector = matrixToVectorOfVectors(V);
  Eigen::Vector3d centroid = getCentroid(meshVector);

  for(size_t i = 0; i < meshVector.size(); i++){
    meshVector[i] = cartesianToSpherical(meshVector[i], centroid);
  }
  
  std::vector<double> rhosA = rhos(meshVector);
  std::vector<std::vector<int>> indexBins;

  std::vector<std::vector<double>> bins = constructBins(rhosA, message.size(), indexBins);
  std::vector<std::vector<double>> normalizedBins(bins.size());
  
  for(size_t i = 0; i < bins.size(); i++){
    normalizeRhosBin(bins[i], normalizedBins[i]);
  }

  for(int t = 0; t < message.size(); t++){
    if(bins[t].size() > 0){
      if(moyBin(normalizedBins[t]) > 0.5){
        message[t] = 1;
      }else{
        message[t] = 0;
      }
    }
  }

  /*for(size_t i = 0; i < message.size(); i++){
    std::cout<<message[i]<<std::endl;
  }*/
}



double RMSE(const Eigen::MatrixXd& predicted, const Eigen::MatrixXd& actual) {
    Eigen::MatrixXd diff = predicted - actual;

    double sumOfSquares = (diff.array() * diff.array()).sum();

    double meanOfSquares = sumOfSquares / (predicted.rows() * predicted.cols());

    double rmse = std::sqrt(meanOfSquares);

    return rmse;
}



Eigen::MatrixXd V; // Les coordonnées des sommets
Eigen::MatrixXi F; // Les faces du maillage

Eigen::MatrixXd V2;
Eigen::MatrixXd F2;

int main(int argc, char *argv[])
{
  igl::readOFF("avion_n.off", V, F);
  igl::readOFF("avion_n.off", V2, F2);


  std::vector<int> m; m.push_back(0); m.push_back(0); m.push_back(1); m.push_back(0); m.push_back(0); m.push_back(1); m.push_back(0); m.push_back(0); 

  /*for(size_t i = 0; i < m.size(); i++){
    m[i] = 0;
  }*/
  std::vector<int> m2(8);
  //insertMessage(V, m, 0.25, 0.2);

  //std::cout<<RMSE(V,V2)<<std::endl;
  //extractMessage(V, m2, 0.1);
  // Plot the mesh
  igl::opengl::glfw::Viewer viewer;
  viewer.data().set_mesh(V, F);
  viewer.data().set_face_based(true);

  viewer.launch();
}
