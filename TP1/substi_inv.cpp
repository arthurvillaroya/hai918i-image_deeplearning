#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    int cle;
    
    if (argc != 4){
        printf("Usage: ImageIn.pgm ImageOut.pgm K\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s", cNomImgLue) ;
    sscanf (argv[2],"%s", cNomImgEcrite);
    sscanf (argv[3],"%d", &cle);
    

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    srand(cle);

    int susbstitu[nTaille];
    for(int i = 0; i < nTaille; i++){
        susbstitu[i] = rand() % 256;
    }

    for (int i = nTaille - 1; i > 0; i--){
        ImgOut[i] = (ImgIn[i] - ImgIn[i-1] - susbstitu[i]) % 256;
    } 

    ImgOut[0] = (ImgIn[0] - susbstitu[0])%256; 

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

   return 0;
}