#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    if (argc != 3){
        printf("Usage: ImageIn.pgm ImgOut.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s", cNomImgLue) ;
    sscanf (argv[2],"%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut, *ImgInt;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(ImgInt, OCTET, nTaille);

    double entropie_min = 10000;
    int true_key;

    for(int key = 0; key < 256; key++){
        
        std::cout<<"Testing key : "<<key<<std::endl;
        srand(key);

        int susbstitu[nTaille];
        for(int i = 0; i < nTaille; i++){
            susbstitu[i] = rand() % 256;
        }

        for (int i = nTaille - 1; i > 0; i--){
            ImgInt[i] = (ImgIn[i] - ImgIn[i-1] - susbstitu[i]) % 256;
        } 

        ImgInt[0] = (ImgIn[0] - susbstitu[0])%256;

        int histo[256];
        for (int i = 0; i < 256; i++){
            histo[i] = 0;
        }

        for(int i = 0; i < nTaille; i++){
            histo[ImgInt[i]]++;
        }

        double entropie = 0;
        double temp;
        double pi;
        for(int i = 0; i < 256; i++){
            if(histo[i] == 0){
                temp = 0;
            }
            else{
                pi = double(histo[i])/nTaille;
                temp = pi * log2(pi);
                entropie -= temp;
            }
        }

        std::cout<<"ENTROPIE INT : "<<entropie<<std::endl;
        if(entropie_min > entropie){
            entropie_min = entropie;
            std::cout<<"ENTRPIE MIN : "<<entropie_min<<std::endl;
            for(int i = 0; i < nTaille; i++){
                ImgOut[i] = ImgInt[i];
            }
            true_key = key;
        } 
    }
    std::cout<<"THE KEY IS : "<<true_key<<std::endl;
    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

   return 0;
}