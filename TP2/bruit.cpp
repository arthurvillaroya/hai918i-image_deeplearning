#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
#include "AES.h"

void intToBinaryArray(int number, int binaryArray[8]) {
    for (int i = 7; i >= 0; i--) {
        binaryArray[i] = (number >> i) & 1;
    }
}

int binaryArrayToInt(const int binaryArray[8]) {
    int result = 0;
    
    for (int i = 0; i < 8; i++) {
        result = (result << 1) | binaryArray[i];
    }
    
    return result;
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    //int cle;
    
    if (argc != 3){
        printf("Usage: ImageIn.pgm ImageOut.pgm K\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s", cNomImgLue) ;
    sscanf (argv[2],"%s", cNomImgEcrite);
    //sscanf (argv[3],"%d", &cle);
    

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for(int i = 0; i < nTaille; i++){
        ImgOut[i] = ImgIn[i];;
    }

    int ValuePixel[8];
    for(unsigned int i = 0; i < nH; i = i + 20){
        for(unsigned int j = 0; j < nW; j = j + 20){
            int aleaI = std::rand() % 5;
            int aleaJ = std::rand() % 5;
            
            intToBinaryArray(ImgOut[i*nW + j], ValuePixel);

            int bitASwitch = std::rand() % 9;

            if(ValuePixel[bitASwitch] == 0){ValuePixel[bitASwitch] = 1;}
            else{ValuePixel[bitASwitch] = 0;}

            ImgOut[i*nW + j] = binaryArrayToInt(ValuePixel);
        }
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

   return 0;
}