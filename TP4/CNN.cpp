#include <stdio.h>
#include "image_ppm.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <fstream>

//passe bas
float moyenneur[9] =
    {1.0/9.0,	1.0/9.0,	1.0/9.0,
	1.0/9.0,	1.0/9.0,	1.0/9.0,
	1.0/9.0,	1.0/9.0,	1.0/9.0};

float gaussien[9] =
    {1.0/16.0,	2.0/16.0,	1.0/16.0,
	2.0/16.0,	4.0/16.0,	2.0/16.0,
	1.0/16.0,	2.0/16.0,	1.0/16.0};


//passe Haut
float haut[9] =
            {0,	-1,	0,
            -1,	5,	-1,
            0,	-1,	0};

float emboss[9] =
            {-2,	-1,	0,
            -1,	1,	1,
            0,	1,	2};


float laplaceDiag[9] =
            {1,	1,	1,
            1,	-8,	1,
            1,	1,	1};

unsigned char GetPixel(OCTET* img, int x, int y, int w, int c) {
    return img[x * w + y + 3 + c];
}

int ApplyFilter(OCTET* img, int x, int y, int w, float filter[], int channel) {
    float value = 0.0;
    for(int i = -1; i <= 1; i++) {
        for(int j = -1; j <= 1; j++) {
            value += GetPixel(img, x + i*3, y + j*3, w, channel) * filter[(i+1)*3 + (j+1)];
        }
    }

    if (value < 0.0) value = 0.0;
    if (value > 255.0) value = 255.0;

    return (int)value;
}

void ApplyFilterOnAllImage(OCTET* ImgnIn, OCTET* ImgOut, float filter[], int taille){
    for(int x = 3; x < taille*3-3;  x = x + 3){
        for(int y = 3; y < taille*3-3;  y = y + 3){
            ImgOut[x * taille + 3 + y] = ApplyFilter(ImgnIn, x, y, taille, filter, 0);
            ImgOut[x * taille + 3 + y + 1] = ApplyFilter(ImgnIn, x, y, taille, filter, 1);
            ImgOut[x * taille + 3 + y + 2] = ApplyFilter(ImgnIn, x, y, taille, filter, 2);
        }    
    }
}

void pooling(OCTET* ImgIn, OCTET* ImgOut, int taille){
    for (int i = 0; i < taille; ++i) {
        for (int j = 0; j < taille; ++j) {
            int x1 = j * 2;
            int x2 = x1 + 1;
            int y1 = i * 2;
            int y2 = y1 + 1;

            unsigned char max_R = 0;
            unsigned char max_G = 0;
            unsigned char max_B = 0;

            for (int y = y1; y <= y2; ++y) {
                for (int x = x1; x <= x2; ++x) {
                    // Indice dans le tableau de l'image d'entrée
                    int index_entree = (y * taille + x ) * 3;

                    // Récupérer les valeurs des canaux R, G et B du pixel
                    unsigned char R = ImgIn[index_entree];
                    unsigned char G = ImgIn[index_entree + 1];
                    unsigned char B = ImgIn[index_entree + 2];

                    // Mettre à jour les valeurs maximales
                    if (R > max_R) max_R = R;
                    if (G > max_G) max_G = G;
                    if (B > max_B) max_B = B;
                }
            }

            // Indice dans le tableau de l'image de sortie
            int index_sortie = (i * taille/2 + j) * 3;

            // Placer les valeurs maximales dans l'image de sortie
            ImgOut[index_sortie] = max_R;
            ImgOut[index_sortie + 1] = max_G;
            ImgOut[index_sortie + 2] = max_B;
        }
    }
}

void pushInVector(OCTET* ImgOutF, OCTET* In, int &cpt){
    for(int i = 0; i < 128; i++){
        for(int j = 0; j < 128; j++){
            ImgOutF[cpt] = In[i*128+j];
            cpt++;
        }
    }
}

void operationFinale(std::vector<OCTET*> Imagettes, OCTET* ImgOutF){
    int cpt = 0;
    std::cout<<Imagettes.size()<<std::endl;
    for(int i = 0; i < Imagettes.size(); i++){
        pushInVector(ImgOutF, Imagettes[i], cpt);
    }
}

unsigned char* readBinary(const char* path, int id)
{
    const int imageWidth = 32;
    const int imageHeight = 32;
    const int channels = 3;
    unsigned char *out;
    allocation_tableau(out, unsigned char, imageWidth*imageHeight*channels);

    // Open the binary file for reading
    std::ifstream file(path, std::ios::binary);

    if (!file) {
        std::cerr << "Failed to open the binary file." << std::endl;
        return NULL;
    }

    const int imageOffset = id * (1 + imageWidth * imageHeight * channels);

    // Seek to the beginning of the desired image
    file.seekg(imageOffset);

    // Read the label and pixel values for the first image
    uint8_t label;
    std::vector<uint8_t> pixelData(imageWidth * imageHeight * channels);

    file.read(reinterpret_cast<char*>(&label), sizeof(label));
    file.read(reinterpret_cast<char*>(pixelData.data()), pixelData.size());

    // Populate the ImageBase instance with pixel values
    for (int i = 0; i < imageHeight; ++i) {
        for (int j = 0; j < imageWidth; ++j) {
            for (int c = 0; c < channels; ++c) {
                int index = c * imageWidth * imageHeight + i * imageWidth + j;
                out[(i * imageWidth + j) * channels + c] = pixelData[index];
            }
        }
    }

    
    return out;
}

int main(int argc, char* argv[]){
    int nH, nW, nTaille;
    char cNomImgLue1[250];

    OCTET* ImgIn;
    if (argc != 2) {
        printf("Usage: ImageIn1.ppm \n"); 
        exit (1) ;
    }

   sscanf (argv[1],"%s",cNomImgLue1) ;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue1, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille*3);
    lire_image_ppm(cNomImgLue1, ImgIn, nH * nW);
    
    int dataSetSize = 1;
    int taille = 512*512*3;

    /*for(int i = 0; i < dataSetSize; i++){
        OCTET *ImgIn;
        allocation_tableau(ImgIn, OCTET, taille);

        std::streampos imagePosition = i * taille;

        unsigned char* image = readBinary("../images/data_batch_1.bin", i);
        for(int i = 0; i < taille; i++){

            ImgIn[i] = image[i];
        }
    }   */
        std::vector<OCTET*> imagettes;
       //Moyenne0
        OCTET *ImgOut1;
        allocation_tableau(ImgOut1, OCTET, 512*512*3);
        ApplyFilterOnAllImage(ImgIn, ImgOut1, moyenneur, 512);
        OCTET *ImgOut1Poo;
        allocation_tableau(ImgOut1Poo, OCTET, 512*512*3);
        pooling(ImgOut1, ImgOut1Poo, 512);
        free(ImgOut1);
        //ecrire_image_ppm("chatMoy.ppm", ImgOut1, 512, 512);
        
            OCTET *ImgOut11;
            allocation_tableau(ImgOut11, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut1Poo, ImgOut11, moyenneur, 256);
            OCTET *ImgOut11Poo;
            allocation_tableau(ImgOut11Poo, OCTET, 256*256*3);
            pooling(ImgOut11, ImgOut11Poo, 256);
            imagettes.push_back(ImgOut11Poo);
            ecrire_image_ppm("chatMoy.ppm", ImgOut11Poo, 128, 128);
            
            OCTET *ImgOut21;
            allocation_tableau(ImgOut21, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut1Poo, ImgOut21, gaussien, 256);
            OCTET *ImgOut21Poo;
            allocation_tableau(ImgOut21Poo, OCTET, 256*256*3);
            pooling(ImgOut21, ImgOut21Poo, 256);
            imagettes.push_back(ImgOut21Poo);

            OCTET *ImgOut31;
            allocation_tableau(ImgOut31, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut1Poo, ImgOut31, haut, 256);
            OCTET *ImgOut31Poo;
            allocation_tableau(ImgOut31Poo, OCTET, 256*256*3);
            pooling(ImgOut31, ImgOut31Poo, 256);
            imagettes.push_back(ImgOut31Poo);

            OCTET *ImgOut41;
            allocation_tableau(ImgOut41, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut1Poo, ImgOut41, emboss, 256);
            OCTET *ImgOut41Poo;
            allocation_tableau(ImgOut41Poo, OCTET, 256*256*3);
            pooling(ImgOut41, ImgOut41Poo, 256);
            imagettes.push_back(ImgOut41Poo);

            OCTET *ImgOut51;
            allocation_tableau(ImgOut51, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut1Poo, ImgOut51, laplaceDiag, 256);
            OCTET *ImgOut51Poo;
            allocation_tableau(ImgOut51Poo, OCTET, 256*256*3);
            pooling(ImgOut51, ImgOut51Poo, 256);
            imagettes.push_back(ImgOut51Poo);







        //Gaussien
        OCTET *ImgOut2;
        allocation_tableau(ImgOut2, OCTET, 512*512*3);
        ApplyFilterOnAllImage(ImgIn, ImgOut2, gaussien, 512);
        OCTET *ImgOut2Poo;
        allocation_tableau(ImgOut2Poo, OCTET, 512*512*3);
        pooling(ImgOut2, ImgOut2Poo, 512);

            OCTET *ImgOut12;
            allocation_tableau(ImgOut12, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut2, ImgOut12, moyenneur, 256);
            OCTET *ImgOut12Poo;
            allocation_tableau(ImgOut12Poo, OCTET, 256*256*3);
            pooling(ImgOut12, ImgOut12Poo, 256);
            imagettes.push_back(ImgOut12Poo);
            
            OCTET *ImgOut22;
            allocation_tableau(ImgOut22, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut2, ImgOut22, gaussien, 256);
            OCTET *ImgOut22Poo;
            allocation_tableau(ImgOut22Poo, OCTET, 256*256*3);
            pooling(ImgOut22, ImgOut22Poo, 256);
            imagettes.push_back(ImgOut22Poo);

            OCTET *ImgOut32;
            allocation_tableau(ImgOut32, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut2, ImgOut32, haut, 256);
            OCTET *ImgOut32Poo;
            allocation_tableau(ImgOut32Poo, OCTET, 256*256*3);
            pooling(ImgOut32, ImgOut32Poo, 256);
            imagettes.push_back(ImgOut32Poo);

            OCTET *ImgOut42;
            allocation_tableau(ImgOut42, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut2, ImgOut42, emboss, 256);
            OCTET *ImgOut42Poo;
            allocation_tableau(ImgOut42Poo, OCTET, 256*256*3);
            pooling(ImgOut42, ImgOut42Poo, 256);
            imagettes.push_back(ImgOut42Poo);

            OCTET *ImgOut52;
            allocation_tableau(ImgOut52, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut2, ImgOut52, laplaceDiag, 256);
            OCTET *ImgOut52Poo;
            allocation_tableau(ImgOut52Poo, OCTET, 256*256*3);
            pooling(ImgOut52, ImgOut52Poo, 256);
            imagettes.push_back(ImgOut52Poo);






        //Haut
        OCTET *ImgOut3;
        allocation_tableau(ImgOut3, OCTET, 512*512*3);
        ApplyFilterOnAllImage(ImgIn, ImgOut3, haut, 512);
        OCTET *ImgOut3Poo;
        allocation_tableau(ImgOut3Poo, OCTET, 512*512*3);
        pooling(ImgOut3, ImgOut3Poo, 512);


            OCTET *ImgOut13;
            allocation_tableau(ImgOut13, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut3, ImgOut13, moyenneur, 256);
            OCTET *ImgOut13Poo;
            allocation_tableau(ImgOut13Poo, OCTET, 256*256*3);
            pooling(ImgOut13, ImgOut13Poo, 256);
            imagettes.push_back(ImgOut13Poo);
            
            OCTET *ImgOut23;
            allocation_tableau(ImgOut23, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut3, ImgOut23, gaussien, 256);
            OCTET *ImgOut23Poo;
            allocation_tableau(ImgOut23Poo, OCTET, 256*256*3);
            pooling(ImgOut23, ImgOut23Poo, 256);
            imagettes.push_back(ImgOut23Poo);

            OCTET *ImgOut33;
            allocation_tableau(ImgOut33, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut3, ImgOut33, haut, 256);
            OCTET *ImgOut33Poo;
            allocation_tableau(ImgOut33Poo, OCTET, 256*256*3);
            pooling(ImgOut33, ImgOut33Poo, 256);
            imagettes.push_back(ImgOut33Poo);

            OCTET *ImgOut43;
            allocation_tableau(ImgOut43, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut3, ImgOut43, emboss, 256);
            OCTET *ImgOut43Poo;
            allocation_tableau(ImgOut43Poo, OCTET, 256*256*3);
            pooling(ImgOut43, ImgOut43Poo, 256);
            imagettes.push_back(ImgOut43Poo);

            OCTET *ImgOut53;
            allocation_tableau(ImgOut53, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut3, ImgOut53, laplaceDiag, 256);
            OCTET *ImgOut53Poo;
            allocation_tableau(ImgOut53Poo, OCTET, 256*256*3);
            pooling(ImgOut53, ImgOut53Poo, 256);
            imagettes.push_back(ImgOut53Poo);



        //Emboos
        OCTET *ImgOut4;
        allocation_tableau(ImgOut4, OCTET, 512*512*3);
        ApplyFilterOnAllImage(ImgIn, ImgOut4, emboss, 512);
        OCTET *ImgOut4Poo;
        allocation_tableau(ImgOut4Poo, OCTET, 512*512*3);
        pooling(ImgOut4, ImgOut4Poo, 512);
        //ecrire_image_ppm("avionEmboss.ppm", ImgOut4, 512, 512);


            OCTET *ImgOut14;
            allocation_tableau(ImgOut14, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut4, ImgOut14, moyenneur, 256);
            OCTET *ImgOut14Poo;
            allocation_tableau(ImgOut14Poo, OCTET, 256*256*3);
            pooling(ImgOut14, ImgOut14Poo, 256);
            imagettes.push_back(ImgOut14Poo);
            
            OCTET *ImgOut24;
            allocation_tableau(ImgOut24, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut4, ImgOut24, gaussien, 256);
            OCTET *ImgOut24Poo;
            allocation_tableau(ImgOut24Poo, OCTET, 256*256*3);
            pooling(ImgOut24, ImgOut24Poo, 256);
            imagettes.push_back(ImgOut24Poo);

            OCTET *ImgOut34;
            allocation_tableau(ImgOut34, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut4, ImgOut34, haut, 256);
            OCTET *ImgOut34Poo;
            allocation_tableau(ImgOut34Poo, OCTET, 256*256*3);
            pooling(ImgOut34, ImgOut34Poo, 256);
            imagettes.push_back(ImgOut34Poo);

            OCTET *ImgOut44;
            allocation_tableau(ImgOut44, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut4, ImgOut44, emboss, 256);
            OCTET *ImgOut44Poo;
            allocation_tableau(ImgOut44Poo, OCTET, 256*256*3);
            pooling(ImgOut44, ImgOut44Poo, 256);
            imagettes.push_back(ImgOut44Poo);

            OCTET *ImgOut54;
            allocation_tableau(ImgOut54, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut4, ImgOut54, laplaceDiag, 256);
            OCTET *ImgOut54Poo;
            allocation_tableau(ImgOut54Poo, OCTET, 256*256*3);
            pooling(ImgOut54, ImgOut54Poo, 256);
            imagettes.push_back(ImgOut54Poo);



        //laplaceDiag
        OCTET *ImgOut5;
        allocation_tableau(ImgOut5, OCTET, 512*512*3);
        ApplyFilterOnAllImage(ImgIn, ImgOut5, laplaceDiag, 512);
        OCTET *ImgOut5Poo;
        allocation_tableau(ImgOut5Poo, OCTET, 512*512*3);
        pooling(ImgOut5, ImgOut5Poo, 512);
        //ecrire_image_ppm("bateauLaplace.ppm", ImgOut5, 512, 512);
            
            OCTET *ImgOut15;
            allocation_tableau(ImgOut15, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut5, ImgOut15, moyenneur, 256);
            OCTET *ImgOut15Poo;
            allocation_tableau(ImgOut15Poo, OCTET, 256*256*3);
            pooling(ImgOut15, ImgOut15Poo, 256);
            imagettes.push_back(ImgOut15Poo);
            
            OCTET *ImgOut25;
            allocation_tableau(ImgOut25, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut5, ImgOut25, gaussien, 256);
            OCTET *ImgOut25Poo;
            allocation_tableau(ImgOut25Poo, OCTET, 256*256*3);
            pooling(ImgOut25, ImgOut25Poo, 256);
            imagettes.push_back(ImgOut25Poo);

            OCTET *ImgOut35;
            allocation_tableau(ImgOut35, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut5, ImgOut35, haut, 256);
            OCTET *ImgOut35Poo;
            allocation_tableau(ImgOut35Poo, OCTET, 256*256*3);
            pooling(ImgOut35, ImgOut35Poo, 256);
            imagettes.push_back(ImgOut35Poo);

            OCTET *ImgOut45;
            allocation_tableau(ImgOut45, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut5, ImgOut45, emboss, 256);
            OCTET *ImgOut45Poo;
            allocation_tableau(ImgOut45Poo, OCTET, 256*256*3);
            pooling(ImgOut45, ImgOut45Poo, 256);
            imagettes.push_back(ImgOut45Poo);

            OCTET *ImgOut55;
            allocation_tableau(ImgOut55, OCTET, 256*256*3);
            ApplyFilterOnAllImage(ImgOut5, ImgOut55, laplaceDiag, 256);
            OCTET *ImgOut55Poo;
            allocation_tableau(ImgOut55Poo, OCTET, 256*256*3);
            pooling(ImgOut55, ImgOut55Poo, 256);
            imagettes.push_back(ImgOut55Poo);
        
        OCTET* ImgOutF;
        allocation_tableau(ImgOutF, OCTET, 128*128*25*3);
        operationFinale(imagettes, ImgOutF);

        //ecrire_image_ppm("vecteurBateau.ppm", ImgOutF, 360, 360);

    return 0;
}
    

