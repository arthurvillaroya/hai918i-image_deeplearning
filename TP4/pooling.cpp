#include <stdio.h>
#include "image_ppm.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <fstream>

//passe bas
float moyenneur[9] =
    {1.0/9.0,	1.0/9.0,	1.0/9.0,
	1.0/9.0,	1.0/9.0,	1.0/9.0,
	1.0/9.0,	1.0/9.0,	1.0/9.0};

float gaussien[9] =
    {1.0/16.0,	2.0/16.0,	1.0/16.0,
	2.0/16.0,	4.0/16.0,	2.0/16.0,
	1.0/16.0,	2.0/16.0,	1.0/16.0};


//passe Haut
float haut[9] =
            {0,	-1,	0,
            -1,	5,	-1,
            0,	-1,	0};

float emboss[9] =
            {-2,	-1,	0,
            -1,	1,	1,
            0,	1,	2};


float laplaceDiag[9] =
            {1,	1,	1,
            1,	-8,	1,
            1,	1,	1};

unsigned char GetPixel(OCTET* img, int x, int y, int w, int c) {
    return img[x * w + y + 3 + c];
}

int ApplyFilter(OCTET* img, int x, int y, int w, float filter[], int channel) {
    float value = 0.0;
    for(int i = -1; i <= 1; i++) {
        for(int j = -1; j <= 1; j++) {
            value += GetPixel(img, x + i*3, y + j*3, w, channel) * filter[(i+1)*3 + (j+1)];
        }
    }

    if (value < 0.0) value = 0.0;
    if (value > 255.0) value = 255.0;

    return (int)value;
}

void ApplyFilterOnAllImage(OCTET* ImgnIn, OCTET* ImgOut, float filter[]){
    for(int x = 3; x < 32*3-3;  x = x + 3){
        for(int y = 3; y < 32*3-3;  y = y + 3){
            ImgOut[x * 32 + 3 + y] = ApplyFilter(ImgnIn, x, y, 32, filter, 0);
            ImgOut[x * 32 + 3 + y + 1] = ApplyFilter(ImgnIn, x, y, 32, filter, 1);
            ImgOut[x * 32 + 3 + y + 2] = ApplyFilter(ImgnIn, x, y, 32, filter, 2);
        }    
    }
}

unsigned char* readBinary(const char* path, int id)
{
    const int imageWidth = 32;
    const int imageHeight = 32;
    const int channels = 3;
    unsigned char *out;
    allocation_tableau(out, unsigned char, imageWidth*imageHeight*channels);

    // Open the binary file for reading
    std::ifstream file(path, std::ios::binary);

    if (!file) {
        std::cerr << "Failed to open the binary file." << std::endl;
        return NULL;
    }

    const int imageOffset = id * (1 + imageWidth * imageHeight * channels);

    // Seek to the beginning of the desired image
    file.seekg(imageOffset);

    // Read the label and pixel values for the first image
    uint8_t label;
    std::vector<uint8_t> pixelData(imageWidth * imageHeight * channels);

    file.read(reinterpret_cast<char*>(&label), sizeof(label));
    file.read(reinterpret_cast<char*>(pixelData.data()), pixelData.size());

    // Populate the ImageBase instance with pixel values
    for (int i = 0; i < imageHeight; ++i) {
        for (int j = 0; j < imageWidth; ++j) {
            for (int c = 0; c < channels; ++c) {
                int index = c * imageWidth * imageHeight + i * imageWidth + j;
                out[(i * imageWidth + j) * channels + c] = pixelData[index];
            }
        }
    }

    
    return out;
}

int main(int argc, char* argv[]){
    int nH, nW, nTaille;
    char cNomImgLue1[250];

    OCTET* ImgIn;
    if (argc != 2) {
        printf("Usage: ImageIn1.ppm \n"); 
        exit (1) ;
    }

    sscanf (argv[1],"%s",cNomImgLue1) ;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue1, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille*3);
    lire_image_ppm(cNomImgLue1, ImgIn, nH * nW);
    OCTET* ImgOut;
    allocation_tableau(ImgOut, OCTET, (nTaille*3));

    for (int i = 0; i < 32; ++i) {
        for (int j = 0; j < 32; ++j) {
            int x1 = j * 2;
            int x2 = x1 + 1;
            int y1 = i * 2;
            int y2 = y1 + 1;

            unsigned char max_R = 0;
            unsigned char max_G = 0;
            unsigned char max_B = 0;

            for (int y = y1; y <= y2; ++y) {
                for (int x = x1; x <= x2; ++x) {
                    // Indice dans le tableau de l'image d'entrée
                    int index_entree = (y * 32 + x )* 3;

                    // Récupérer les valeurs des canaux R, G et B du pixel
                    unsigned char R = ImgIn[index_entree];
                    unsigned char G = ImgIn[index_entree + 1];
                    unsigned char B = ImgIn[index_entree + 2];

                    // Mettre à jour les valeurs maximales
                    if (R > max_R) max_R = R;
                    if (G > max_G) max_G = G;
                    if (B > max_B) max_B = B;
                }
            }

            // Indice dans le tableau de l'image de sortie
            int index_sortie = (i * 16 + j) * 3;

            // Placer les valeurs maximales dans l'image de sortie
            ImgOut[index_sortie] = max_R;
            ImgOut[index_sortie + 1] = max_G;
            ImgOut[index_sortie + 2] = max_B;
        }
    }

    ecrire_image_ppm("cNomImgEcrite.ppm", ImgOut,  nH/2, nW/2);
    free(ImgIn);
    return 0;
}